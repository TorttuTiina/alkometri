import { Component, OnInit } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  genders = [];
  times = [];
  bottles = [];

  weight: number;
  gender: string;
  time: number;
  bottle: number;
  promilles: number;

  constructor() {}

  ngOnInit() {
    this.genders.push('Male');
    this.genders.push('Female');

    for (let i = 1; i <= 24; i++) {
      this.times.push(i);
    }

    for (let i = 1; i <= 24; i++) {
      this.bottles.push(i);
    }
  }

  calculate() {
    const litres = this.bottle * 0.33;
    let grams = litres * 8 * 4.5;
    const burning = this.weight / 10;
    grams = grams - burning * this.time;
    console.log(this.gender);
    if (this.gender === 'Male') {
      this.promilles = grams / (this.weight * 0.7);
    } else {
      this.promilles = grams / (this.weight * 0.6);
    }
  }

}
